import os

with os.scandir('C:\\Users\\Job\\Downloads\\ftp-master') as it:
    for entry in it:
        if entry.name.endswith(".json") and entry.is_file():
            print(entry.name, entry.path)
            #input file
            fin = open(entry.path, "rt")
            #output file to write the result to
            outname = "new_" + entry.name
            fout = open(outname, "wt")
            #for each line in the input file
            for line in fin:
                #read replace the string and write to output file
                #fout.write(line.replace('\'', '\"'))
                fout.write(line.replace('False', '\"False\"'))
            #close input and output files
            fin.close()
            fout.close()