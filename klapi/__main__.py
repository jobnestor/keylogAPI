#!/usr/bin/env python3
"""Simple APP to recieve keylogging data"""

import time
import flask
import random

from flask import *

app = flask.Flask(__name__)
app.config.from_pyfile('config.py')

music = 1
trialType = 2

def chooseStimuli(music):
    if music == 1:
        return flask.render_template('formNews.j2', cfg=app.config)
    elif music == 2:
        return flask.render_template('formMusic.j2', cfg=app.config)
    elif music == 3:
        return flask.render_template('form.j2', cfg=app.config)
    elif music == 4:
        return flask.render_template('formMusic2.j2', cfg=app.config)
    elif music == 5:
        return flask.render_template('formEmpty.j2', cfg=app.config)
    elif music == 6:
        return flask.render_template('formNews2.j2', cfg=app.config)
    else:
        return flask.render_template('form.j2', cfg=app.config)


def write_data(data):
    """Simple wrapper around f.write to write data to a file

    The function should guarantee unique sequential filenames.
    """
    global music
    global trialType
    epoch = time.time()
    cookies = request.cookies
    userid = cookies.get("studypartID")
    # NB! Extra /// in filenames are not a problem in Linux.
    # So better safe than sorry.
    ip_address = flask.request.remote_addr
    filename = f"{app.config['DATA_DIR']}/klapi.{epoch}.IP:{ip_address}.Userid:{userid}.Music:{music}.{trialType}.json"

    datafile = open(filename, "w")
    datafile.write(str(data))
    datafile.close()

@app.route('/', methods=['GET'])
def home():
    """Handle the front page"""
    randomnumber = random.randint(0, 500)	
    cookies = request.cookies	
    userid = cookies.get("studypartID")	
    res = make_response(flask.render_template('index.j2', cfg=app.config, userid=userid), 200)	
        
    if userid != None:	
        return res	
    else:	
        res.set_cookie(	
            "studypartID", 	
            value=str(randomnumber), 	
            max_age=999999999, 	
            expires=None, 	
            path='/', 	
            domain=None, 	
            secure=False, 	
            httponly=False, 	
            samesite=None	
        )	
        return res

@app.route('/formControl', methods=['GET'])
def formControl():
    """Handle initial form entry for pretest"""
    global music
    global trialType
    trialType = random.randrange(1, 3)
    if trialType == 1:
        """ Standard pretest stimuli posttest """
        return flask.render_template('form.j2', cfg=app.config)
    elif trialType == 2:
        music = random.randrange(1, 7)
        if music == 1:
            return flask.render_template('formNews.j2', cfg=app.config)
        elif music == 2:
            return flask.render_template('formMusic.j2', cfg=app.config)
        elif music == 3:
            return flask.render_template('form.j2', cfg=app.config)
        elif music == 4:
            return flask.render_template('formMusic2.j2', cfg=app.config)
        elif music == 5:
            return flask.render_template('formEmpty.j2', cfg=app.config)
        elif music == 6:
            return flask.render_template('formNews2.j2', cfg=app.config)
        else:
            return flask.render_template('form.j2', cfg=app.config)
    else:
        flask.render_template('index.j2')

@app.route('/posttest', methods=['GET', 'POST'])
def posttest():
    """Handle form exit for posttest """
    return flask.render_template('posttest.j2', cfg=app.config)

@app.route('/form', methods=['GET', 'POST'])
def form():
    """Handle the input form where logging occurs"""
    music = random.randrange(1, 7)
    return chooseStimuli(music)


@app.route('/api/data_receiver', methods=['POST'])
def api_data_receiver():
    """Handle incoming data"""

    write_data(flask.request.get_json())
    return flask.Response(status=200)

@app.route('/api/echo', methods=['GET','POST'])
def api_echo():
    """Handle incoming data"""
    return flask.render_template('success.j2', cfg=app.config)

# I don't wanna run with bad SSL-certificates when developing.
# URGH! This way https won't work if running in DEBUG mode,
# this way you should notice before launch debug code into
# production.
if app.config['DEBUG']:
    app.run(host="0.0.0.0")
else:
    app.run(ssl_context='adhoc', host="0.0.0.0")