// /var/www/html/js/timing.js

let EventBuffer = new Array()
const last_event = null

function sendEvents (pending) {
  const request = new XMLHttpRequest()
  request.open('POST', '/api/data_receiver', true)
  request.setRequestHeader('Content-Type', 'application/json')
  request.send(JSON.stringify(pending))
}

function StreamEvent (event) {
  const ev = {
    clockTime: new Date().getTime(),
    timestamp: event.timeStamp,
    altkey: event.altKey,
    metakey: event.metaKey,
    key: event.key,
    keyCode: event.keyCode,
    type: event.type,
    button: event.button,
    buttons: event.buttons,
  }

  EventBuffer.push(ev)
  if (EventBuffer.length >= 100) {
    Pending = EventBuffer
    EventBuffer = new Array()
    sendEvents(Pending)
  }
}

$(document).keydown(StreamEvent)
$(document).keyup(StreamEvent)
$(document).keypress(StreamEvent)
$(document).mousemove(StreamEvent)
$(document).click(StreamEvent)
