#!/usr/bin/env python3
"""Constants to be used for app.config"""
# Bad way to set debug. Use FLASK_APP_DEBUG instead.
# Launch.json handles this in VS Code
DEBUG = True

# Just a silly message to set, to show how j2-templates work.
# Feel free to delete this.
WELCOME_MSG="Study: The Effect of External Stimuli on Continous Keystroke Dynamics"

# Actual config. Where to write incoming keylogger data to
DATA_DIR="/tmp"
