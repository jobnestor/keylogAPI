#!/usr/bin/make -ef

default: dev

requirements:
	python3 -m pip install -r requirements.txt

test-requirements:
	python3 -m pip install -r requirements-test.txt

test: test-requirements
	tox -q

build-requirements:
	python3 -m pip install --upgrade pip
	python3 -m pip install --upgrade build

build: build-requirements test
	python3 -m build

clean:
	find build klapi_jobnestor.egg-info -type f -delete || true
	find * -type d -name __pycache__ -exec rm -rf {}  \; 2> /dev/null || true

distclean: clean
	rm -rf build klapi_jobnestor.egg-info dist venv || true

dev: requirements
	python3 -m klapi


.PHONY: build
