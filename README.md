# keylogAPI
API to receive data from a form that logs keystrokes, used by consenting individuals for research into keystroke dynamics authentication.

# Purpose
biometrics concerns itself with finding out who an unknown subject is; whichis also known as identification. More precisely, identification can be described asthe pro-cess of searching against a biometric enrolment database to find and return the biometricreference identifier(s) attributable to a single individual[https://christoph-busch.de/standards.html#370311]. Within the confines if thispaper, the process of authenticating an individual based onsomething they dois the ob-jective. The behaviour that will be approached, iskeystroke dynamics. Keystroke dynamicsis how a person can authenticate themselves byhowthey type, rather thanwhatthey type.This kind ofbehavioural biometrichas been studied in many papers over the years, suchas  [http://www.jprr.org/index.php/jprr/article/view/427/167].  The  focus  in  this  paper,  will  be  to  answer  whether  external  stimuli  in  theform of loud music and newscasts affects how a data subject types, in such a statisticallysignificant manner that it is worth accounting for or recognising in a keystroke dynamicsauthentication program.Within the context of keystroke dynamics, there are multiple methods of performing theauthentication. One of these methods is calledContinuous Keystroke Dynamicsor (CKD),which is the topic of this paper. CKD is a form of keystroke dynamics that continuouslymeasures the subject whom is typing. Therefore, if someone else was to take over duringa writing session, the recognition of the individual that you measure against would dropbelow a certain threshold, and the confidence in the identity of the user would be reduced.There is not necessarily performed any actions associated with a lowered confidence inpurported identity of the typing subject.Another method would be to dostatic keystroke dynamics, in which a text would be typed,and the keystroke information would be analysed as a whole set and would be factoredinto the authentication decision, thus directly leading to an authentication decision of thetypist.[https://christoph-busch.de/standards.html#370311].In the case of CKD, it may occur that a typist at their keyboard may be influenced byexternal factors that would affect the biometric sampling to such an extent that the CKDauthentication can seem non-mated. In such an event an indicidual can be subject to a falsenon-match, the presence of which is measured by theFalse Non-Match Rate(FNMR). Theaim of this study is therefore to reduce the FNMR of CKD by collecting and analyzingdata that can be used to improve CKD algorithms, thus: Keystroke Dynamics behaviourunder Constrained Conditions (KCC)

# Usage
The study survey form can be found here: http://142.93.238.108:5000/

The / endpoint redirects to the /form endpoint via hyperlink, and informs about the study. 

The endpoints /api/ does nothing

The endpoint /api/data_receiver receives a POST from the keylogging javascript, and calls a data saving method.

The endpoint /api/echo GETs a success messsage after completed forms.

The endpoint /form is the form which sends data to the /api/data_receiver endpoint and the /api/echo endpoint

The endpoint /formControl is an endpoint that renders the form without music, for control groups.

The Json produced by the timing.js script in the /form endpoint looks something like this.
The clocktime is the clocktime registered by the users browser, and the timestamp refers to when the event happened.⊞ Windows
altkey registers the pressing of the alt key, while the metakey registers if a metakey ws pressed (this is different on different OSes) In windows this is the the ⊞ Windows key, but not in firefox.

   ```
    [
   {
      "clockTime":1614882427939,
      "timestamp":11579.509999999573,
      "altkey":false,
      "metakey":false,
      "key":"i",
      "keyCode":73,
      "type":"keydown"
   },
   {
      "clockTime":1614882427939,
      "timestamp":11579.689999998664,
      "altkey":false,
      "metakey":false,
      "key":"i",
      "keyCode":105,
      "type":"keypress"
   },
   {
      "clockTime":1614882427955,
      "timestamp":11595.414999999775,
      "altkey":false,
      "metakey":false,
      "key":"p",
      "keyCode":80,
      "type":"keyup"
   },
   ...
    ]
    ```
